import 'flowbite';
import { Navbar } from 'flowbite-react';

export default function myNavbar() {
    return (

        <Navbar
            fluid={true}
            rounded={true}
        >
            <Navbar.Brand href="#">
                <i className="fa-solid fa-cat fa-2xl"></i>
                <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
                    Cats Lover
                </span>
            </Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse>
                <Navbar.Link
                    href="/"
                    active={true}
                >
                    Home
                </Navbar.Link>
                <Navbar.Link href="/">
                    About
                </Navbar.Link>
                <Navbar.Link href="/">
                    Services
                </Navbar.Link>
                <Navbar.Link href="/">
                    Pricing
                </Navbar.Link>
                <Navbar.Link href="/">
                    Contact
                </Navbar.Link>
            </Navbar.Collapse>
        </Navbar>

    )
}