import React from "react";

function Pagination({ catsPerPage, totalCats, paginate }) {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(totalCats / catsPerPage); i++) {
        pageNumbers.push(i)
    }

    return (<section className="mb-4">
        <ul className="flex justify-center ">
            {pageNumbers.map(number => (
                <li key={number} className="px-3 border-sky-100 border-2 focus:border-sky-500 focus:ring-sky-500" >
                    <a onClick={() => paginate(number)} href="!#">
                        {number}
                    </a>
                </li>
            ))}
        </ul>
    </section>);
}

export default Pagination;