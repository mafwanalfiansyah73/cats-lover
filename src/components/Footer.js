import React from "react"
import { Footer } from "flowbite-react"

export default function MyFooter() {
    return (
        <Footer>
            <Footer.Copyright
                href="#"
                by="Cats Lover™"
                year={2022}
            />
            <Footer.LinkGroup className="mt-3 flex-wrap items-center text-sm sm:mt-0">
                <Footer.Link href="#">
                    About
                </Footer.Link>
                <Footer.Link href="#">
                    Privacy Policy
                </Footer.Link>
                <Footer.Link href="#">
                    Licensing
                </Footer.Link>
                <Footer.Link href="#">
                    Contact
                </Footer.Link>
            </Footer.LinkGroup>
        </Footer>
    )
}