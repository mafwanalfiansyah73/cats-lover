import { Button } from "flowbite-react/lib/esm/components"
import style from './Banner.module.css'
import { useNavigate } from "react-router-dom"

export default function Banner() {
    let navigate = useNavigate()

    const handleClick = () => {
        navigate("/result")
    }

    return (
        <section className={style.banner}>
            <div className={style.banner_form_wrap}>
                <Button gradientMonochrome="teal" type="submit" onClick={handleClick}>
                    Search by Name
                </Button>
            </div>
        </section>
    )
}