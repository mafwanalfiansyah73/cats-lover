import React from "react"
import MyNavbar from "../components/navbar/Navbar"
import style from '../components/banner/Banner.module.css'
import axios from "axios"
import { Label, TextInput, Button } from "flowbite-react/lib/esm/components"
import { useState } from "react"
import { Card, Dropdown } from "flowbite-react/lib/esm/components"

export default function SearchCat() {
    const [input, setInput] = useState('');
    const [catlist, setCatlist] = useState([])

    const handleSubmit = (e) => {
        e.preventDefault();
        axios(`https://api.thecatapi.com/v1/breeds/search?q=${input}`)
            .then((res) => {
                console.log("dibawah ini res.data")
                setCatlist(res.data)
                console.log(catlist)
            })
            .catch(err => console.log(err))
    }
    return (
        <section>
            <MyNavbar />
            <div className={style.banner}>
                <div className={style.banner_form_wrap}>
                    <form onSubmit={handleSubmit} className="flex flex-col text-center  w-[200%] sm:w-[100%]">
                        <div className="mb-2 block ">
                            <Label
                                htmlFor="small"
                                value="Search cats"
                                className=" text-yellow-50 text-xl md:text-3xl"
                            />
                        </div>
                        <div className="flex justify-evenly">
                            <TextInput
                                id="small"
                                type="text"
                                sizing="sm md:md"
                                onChange={(e) => setInput(e.target.value)}
                                placeholder="e.g American Cat"
                                className="mb-2 sm:w-[150%] md:w-[175%] lg:w-[200%] xl:w-[250%]"
                            />
                            <Button gradientMonochrome="teal" type="submit">
                                Search
                            </Button>
                        </div>
                    </form>
                </div>
            </div>
            <div className=" mt-24 mx-4 sm:grid sm:grid-cols-2 sm:gap-4 lg:grid-cols-3">
                {catlist ? catlist.map((data, i) => (
                    <ul key={i}>
                        <li className="my-4">
                            <Card
                                imgAlt="Meaningful alt text for an image that is not purely decorative"

                            >
                                <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white text-center">
                                    {data.name}
                                </h5>
                                <div className=" flex justify-center ">
                                    <Dropdown
                                        label="Detail"
                                        inline={true}
                                    >
                                        <Dropdown.Item>
                                            <p className="font-normal text-gray-700 dark:text-gray-400">
                                                {data.description}
                                            </p>
                                        </Dropdown.Item>
                                    </Dropdown>
                                </div>
                            </Card></li>
                    </ul>
                )) : <h1>Data not Found</h1>}
            </div>
        </section>
    )
}