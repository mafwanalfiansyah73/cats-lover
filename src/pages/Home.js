import MyNavbar from "../components/navbar/Navbar";
import Banner from "../components/banner/Banner";
import axios from "axios"
import { useEffect, useState } from "react"
import Cats from "../components/cats/Cats";
import Pagination from "../components/Pagination";
import MyFooter from "../components/Footer";

export default function Home() {
    const [cats, setCats] = useState([])
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [catsPerPage] = useState(10)

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            const res = await axios(`https://api.thecatapi.com/v1/breeds`)
            setCats(res.data)
            setLoading(false)
            console.log(res.data)
        }

        fetchData()

    }, [])

    const indexOfLastCat = currentPage * catsPerPage
    const indexOfFirstCat = indexOfLastCat - catsPerPage
    const currentCats = cats.slice(indexOfFirstCat, indexOfLastCat)

    const paginate = (pageNumber) => setCurrentPage(pageNumber)
    return (
        <div className="Home">
            <MyNavbar />
            <Banner />
            <Cats cats={currentCats} loading={loading} currentPage={currentPage} />
            <Pagination catsPerPage={catsPerPage} totalCats={cats.length} paginate={paginate} />
            <MyFooter />
        </div>
    )
}