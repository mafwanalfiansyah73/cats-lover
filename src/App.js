import React from "react";
import Home from "./pages/Home";
import { Routes, Route } from "react-router-dom"
import SearchCat from "./pages/SearchCat"
import Error404 from "./pages/Error404"

function App() {

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/!" element={<Home />} />
        <Route path="/result" element={<SearchCat />} />
        <Route path="*" element={<Error404 />} />
      </Routes>
    </div>
  );
}

export default App;
